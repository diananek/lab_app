from utility import get_json_file, get_int_list, get_int, get_float
from openpyxl import load_workbook


def plan_loader():
    result = []
    wb = load_workbook('data.xlsx')
    sheet = wb['План']
    n = sheet.max_row

    for i in range(6, n):
        if sheet['A' + str(i)].value != '+' and sheet['A' + str(i)].value != '-':
            continue

        row = sheet['B' + str(i):'CF' + str(i)][0]
        discipline = {
            'general': {
                'index': row[0].value,
                'title': row[1].value,
                'controlForm': {
                    'exam': get_int_list(row[2].value),
                    'test': get_int_list(row[3].value),
                    'testWithMark': get_int_list(row[4].value),
                    'kp': get_int_list(row[5].value),
                    'kr': get_int_list(row[6].value),
                    'control': get_int_list(row[7].value)
                },
                'testUnits': {
                    'expert': get_int(row[8].value),
                    'fact': get_int(row[9].value)
                },
                'totalAcademicHours': {
                    'expert': get_int(row[11].value),
                    'plan': get_int(row[12].value),
                    'contact': get_int(row[13].value),
                    'selfPaced': get_int(row[14].value),
                    'control': get_int(row[15].value)
                },
                'semesters': {
                    '1': {
                        'testUnits': get_float(row[16].value),
                        'hours': {
                            'total': get_int(row[17].value),
                            'lectures': get_int(row[18].value),
                            'labs': get_int(row[19].value),
                            'practic': get_int(row[20].value),
                            'ksr': get_int(row[21].value),
                            'selfPaced': get_int(row[22].value),
                            'control': get_int(row[23].value)
                        }
                    },
                    '2': {
                        'testUnits': get_float(row[24].value),
                        'hours': {
                            'total': get_int(row[25].value),
                            'lectures': get_int(row[26].value),
                            'labs': get_int(row[27].value),
                            'practic': get_int(row[28].value),
                            'ksr': get_int(row[29].value),
                            'selfPaced': get_int(row[30].value),
                            'control': get_int(row[31].value)
                        },
                    },
                    '3': {
                        'testUnits': get_float(row[32].value),
                        'hours': {
                            'total': get_int(row[33].value),
                            'lectures': get_int(row[34].value),
                            'labs': get_int(row[35].value),
                            'practic': get_int(row[36].value),
                            'ksr': get_int(row[37].value),
                            'selfPaced': get_int(row[38].value),
                            'control': get_int(row[39].value)
                        },
                    },
                    '4': {
                        'testUnits': get_float(row[40].value),
                        'hours': {
                            'total': get_int(row[41].value),
                            'lectures': get_int(row[42].value),
                            'labs': get_int(row[43].value),
                            'practic': get_int(row[44].value),
                            'ksr': get_int(row[45].value),
                            'selfPaced': get_int(row[46].value),
                            'control': get_int(row[47].value)
                        },
                    },
                    '5': {
                        'testUnits': get_float(row[48].value),
                        'hours': {
                            'total': get_int(row[49].value),
                            'lectures': get_int(row[50].value),
                            'labs': get_int(row[51].value),
                            'practic': get_int(row[52].value),
                            'ksr': get_int(row[53].value),
                            'selfPaced': get_int(row[54].value),
                            'control': get_int(row[55].value)
                        },
                    },
                    '6': {
                        'testUnits': get_float(row[56].value),
                        'hours': {
                            'total': get_int(row[57].value),
                            'lectures': get_int(row[58].value),
                            'labs': get_int(row[59].value),
                            'practic': get_int(row[60].value),
                            'ksr': get_int(row[61].value),
                            'selfPaced': get_int(row[62].value),
                            'control': get_int(row[63].value)
                        },
                    },
                    '7': {
                        'testUnits': get_float(row[64].value),
                        'hours': {
                            'total': get_int(row[65].value),
                            'lectures': get_int(row[66].value),
                            'labs': get_int(row[67].value),
                            'practic': get_int(row[68].value),
                            'ksr': get_int(row[69].value),
                            'selfPaced': get_int(row[70].value),
                            'control': get_int(row[71].value)
                        },
                    },
                    '8': {
                        'testUnits': get_float(row[72].value),
                        'hours': {
                            'total': get_int(row[73].value),
                            'lectures': get_int(row[74].value),
                            'labs': get_int(row[75].value),
                            'practic': get_int(row[76].value),
                            'ksr': get_int(row[77].value),
                            'selfPaced': get_int(row[78].value),
                            'control': get_int(row[79].value)
                        },
                    },
                },
                'competences': [e.strip() for e in row[82].value.split(';')],
                'assignedDepartment': {
                    'departmentId': get_int(row[80].value),
                    'departmentTitle': row[81].value
                }
            }
        }

        for k in range(1, 9):
            if discipline['general']['semesters'][str(k)]['hours']['total'] is None:
                discipline['general']['semesters'].pop(str(k))

        result.append(discipline)

    get_json_file(result)
