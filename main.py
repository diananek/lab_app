from utility import get_json_file, get_int_list, get_int, get_float
from openpyxl import load_workbook
import copy


lib = []
discipline = {
    'general': {
        'index': '',
        'title': '',
        'controlForm': {
            'exam': [],
            'test': [],
            'testWithMark': [],
            'kp': [],
            'kr': [],
            'control': []
        },
        'testUnits': {
            'expert': 0,
            'fact': 0
        },
        'totalAcademicHours': {
            'expert': None,
            'plan': None,
            'contact': None,
            'selfPaced': None,
            'control': None
        },
        'semesters': {
            '1': {
                'testUnits': None,
                'hours': {
                    'total': None,
                    'lectures': None,
                    'labs': None,
                    'practic': None,
                    'ksr': None,
                    'selfPaced': None,
                    'control': None
                }
            },
            '2': {
                'testUnits': None,
                'hours': {
                    'total': None,
                    'lectures': None,
                    'labs': None,
                    'practic': None,
                    'ksr': None,
                    'selfPaced': None,
                    'control': None
                },
            },
            '3': {
                'testUnits': None,
                'hours': {
                    'total': None,
                    'lectures': None,
                    'labs': None,
                    'practic': None,
                    'ksr': None,
                    'selfPaced': None,
                    'control': None
                },
            },
            '4': {
                'testUnits': None,
                'hours': {
                    'total': None,
                    'lectures': None,
                    'labs': None,
                    'practic': None,
                    'ksr': None,
                    'selfPaced': None,
                    'control': None
                },
            },
            '5': {
                'testUnits': None,
                'hours': {
                    'total': None,
                    'lectures': None,
                    'labs': None,
                    'practic': None,
                    'ksr': None,
                    'selfPaced': None,
                    'control': None
                },
            },
            '6': {
                'testUnits': None,
                'hours': {
                    'total': None,
                    'lectures': None,
                    'labs': None,
                    'practic': None,
                    'ksr': None,
                    'selfPaced': None,
                    'control': None
                },
            },
            '7': {
                'testUnits': None,
                'hours': {
                    'total': None,
                    'lectures': None,
                    'labs': None,
                    'practic': None,
                    'ksr': None,
                    'selfPaced': None,
                    'control': None
                },
            },
            '8': {
                'testUnits': None,
                'hours': {
                    'total': None,
                    'lectures': None,
                    'labs': None,
                    'practic': None,
                    'ksr': None,
                    'selfPaced': None,
                    'control': None
                },
            },
        },
        'competences': [
            ''
        ],
        'assignedDepartment': {
            'departmentId': 0,
            'departmentTitle': ''
        }
    }
}
literature_dict = {
    "literature": {
        "main": [

        ],
        "additional": [

        ],
        "misc": [

        ]
    }
}


discipline_dict = discipline['general']

wb = load_workbook('data.xlsx')
if 'План' in wb.sheetnames:
    sheet = wb['План']

for i in range(6, 31):
    discipline_dict_copy = copy.deepcopy(discipline_dict)

    discipline_dict_copy['index'] = sheet['B' + str(i)].value
    discipline_dict_copy['title'] = sheet['C' + str(i)].value

    controlForm = ['exam', 'test', 'testWithMark', 'kp', 'kr', 'control']
    for row in sheet['D' + str(i):'I' + str(i)]:
        for k in range(len(row)):
            discipline_dict_copy['controlForm'][controlForm[k]] = get_int_list(row[k].value)

    discipline_dict_copy['testUnits']['expert'] = get_int(sheet['J' + str(i)].value)
    discipline_dict_copy['testUnits']['fact'] = get_int(sheet['K' + str(i)].value)

    totalAcademicHours = ['expert', 'plan', 'contact', 'selfPaced', 'control']
    for row in sheet['M' + str(i):'Q' + str(i)]:
        for k in range(len(row)):
            discipline_dict_copy['totalAcademicHours'][totalAcademicHours[k]] = get_int(row[k].value)

    semesters = ['total', 'lectures', 'labs', 'practic', 'ksr', 'selfPaced', 'control']
    for row in sheet['R' + str(i):'Y' + str(i)]:
        discipline_dict_copy['semesters']['1']['testUnits'] = get_float(row[0].value)
        for k in range(1, len(row) - 1):
            discipline_dict_copy['semesters']['1']['hours'][semesters[k]] = get_int(row[k].value)

    for row in sheet['Z' + str(i):'AG' + str(i)]:
        discipline_dict_copy['semesters']['2']['testUnits'] = get_float(row[0].value)
        for k in range(1, len(row) - 1):
            discipline_dict_copy['semesters']['2']['hours'][semesters[k]] = get_int(row[k].value)

    for row in sheet['AH' + str(i):'AO' + str(i)]:
        discipline_dict_copy['semesters']['3']['testUnits'] = get_float(row[0].value)
        for k in range(1, len(row) - 1):
            discipline_dict_copy['semesters']['3']['hours'][semesters[k]] = get_int(row[k].value)

    for row in sheet['AP' + str(i):'AW' + str(i)]:
        discipline_dict_copy['semesters']['4']['testUnits'] = get_float(row[0].value)
        for k in range(1, len(row) - 1):
            discipline_dict_copy['semesters']['4']['hours'][semesters[k]] = get_int(row[k].value)

    for row in sheet['AX' + str(i):'BE' + str(i)]:
        discipline_dict_copy['semesters']['5']['testUnits'] = get_float(row[0].value)
        for k in range(1, len(row) - 1):
            discipline_dict_copy['semesters']['5']['hours'][semesters[k]] = get_int(row[k].value)

    for row in sheet['BF' + str(i):'BM' + str(i)]:
        discipline_dict_copy['semesters']['6']['testUnits'] = get_float(row[0].value)
        for k in range(1, len(row) - 1):
            discipline_dict_copy['semesters']['6']['hours'][semesters[k]] = get_int(row[k].value)

    for row in sheet['BN' + str(i):'BU' + str(i)]:
        discipline_dict_copy['semesters']['7']['testUnits'] = get_float(row[0].value)
        for k in range(1, len(row) - 1):
            discipline_dict_copy['semesters']['7']['hours'][semesters[k]] = get_int(row[k].value)

    for row in sheet['BV' + str(i):'CC' + str(i)]:
        discipline_dict_copy['semesters']['8']['testUnits'] = get_float(row[0].value)
        for k in range(1, len(row) - 1):
            discipline_dict_copy['semesters']['8']['hours'][semesters[k]] = get_int(row[k].value)

    discipline_dict_copy['competences'] = [e.strip() for e in sheet['CF' + str(i)].value.split(';')]
    discipline_dict_copy['assignedDepartment']['departmentId'] = int(sheet['CD' + str(i)].value)
    discipline_dict_copy['assignedDepartment']['departmentTitle'] = sheet['CE' + str(i)].value

    list_of_value = ['S', 'AA', 'AI', 'AQ', 'AY', 'BG', 'BO', 'BW']
    k = 1
    for e in list_of_value:
        if sheet[e + str(i)].value is None:
            discipline_dict_copy['semesters'].pop(str(k))
        k += 1

    lib.append({'general': discipline_dict_copy, 'literature': literature_dict['literature']})

get_json_file(lib)
