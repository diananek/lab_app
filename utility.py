import json


def get_python_file():
    with open('discipline_example_data.json', encoding='UTF-8') as json_file:
        result = json.load(json_file)
    json_file.close()
    return result


def get_json_file(list_value):
    with open('discipline_example_data.json', 'w', encoding='UTF-8') as json_file:
        result = json.dumps(list_value, ensure_ascii=False, indent=4)
        json_file.write(f'{result}\n')
    json_file.close()


def get_int(value):
    if value is None:
        return None
    else:
        return int(value)


def get_float(value):
    if value is None:
        return None
    else:
        return float(value)


def get_int_list(str_value):
    if str_value is not None:
        return [int(item) for item in list(str_value)]
